<?php

/* Адрес и порт для подключения к SOAP 1С в регионах
  ht-mail.hi-tk.ru:81 или 95.80.66.78:81
  st-mail.st-profit.ru:81 или 149.255.24.150:81
  vlg-mail.profitvolga.ru:81 или 213.234.30.102:81
 */

interface IExchange {

    /**
     * @TODO: Комментарии
     */
    public function getRoutes($managerCode);

    public function getLastActivities($pointCode);

    /**
     * getDebtInfo - Новая система возврата долга контрагентов
     * @code (string) - Код контрагента
     * @return (array) - массив данных о контрагенте
     * array(array(
     *      'name' => '@Наименование организации',
     *      'type' => '@Тип взаиморасчета',
     *      'prepayment'=>'@Аванс',
     *      'debt' => array(array(
     *          'doc_id' => '@Номер накладный',
     *          'doc_price' => '@Сумма долга',
     *          'doc_payday' => '@Дедлайн оплаты',
     *          'doc_date' => '@Дата накладной'
     *      ));
     * ));
     * 
     */
    public function getDebtInfo($code);
    /*
      @return array(array(
      'DeliveryAddressCode' => @string,
      'districtCode' => @string,
      'cityCode' => @string,
      'regionCode' => @string,
      ));
     */

    public function getContactorsRegions();

    /*
      getRegionsList - Получение списка регионов из 1С
      @return array(array(
      'regionCode' => '@string',
      'regionName' => '@string',
      'cities' => array(array(
      'cityCode' => @string,
      'cityName' => @string,
      'districts' => array(array(
      'districtCode' => @string,
      'districtName' => @string))
      ))
      ));
     */

    public function getRegionsList();


    /*
      @return array Массив дискретных скидок
      array(array(
      'discountCode'=>'@discountCode'
      'discountValue@discountValue'
      'discountName'=>'@discountName'
      'discountValueCodediscountValueCode'=>'@discountValueCode'
      ));
     */

    function getDiscreteDiscounts();

    /*
      @return array Массив списка брендов
      array(array(
      'discountCode'=>'@discountCode',
      'discountName'=>'@discountName',
      'discountBrandCode'=>'@discountBrandCode',
      ));
     */

    function getDiscountBrands();

    /*
      @return array Массив списка брендов

      array(array(
      'brandCode'=>'@brandCode',
      'brandName'=>'@brandName'
      ));

     */

    function getManufacturers();

    /*
      @return array Массив скидок контрагентов
      array(array(
      'clientCode'=>'@clientCode',
      'discountBrandCode'=>'@discountBrandCode',
      'discountValue'=>'@discountValue'
      ));

      @clientCode = Код 1С контрагента
      @discountBrandCode = Код 1С бренда скидки
      @discountValue = Размрер скидки

     */

    function getClientDiscounts();


    /*
      @return (array)

      array(
      array(
      'warehouse_id' => (str),
      'productCode' => (str),
      'waitingQuantity' => (int)
      'date' => (date) ))


     */

    function waitingStuff();

    /*
      @param (str) Код пользователя
      @return (bool) Результат запроса,доступен/недоступен
     */

    function checkDiscountsUpdate($clientCode);

    /*
      Функция обмена номенклатурой
      @param (int) - Номер конфигурации сайта инициатора
      @return (mixed) - Результат: Список всей номенклатуры по 1С

      RESULT:
      array(
      array(
      'warehouse_id' => (int) @warehouse_id,
      'count' => (int) @count,
      'products' => (int) array(
      array(
      'brandCode' => @brandCode, 'article'=>@article, 'name'=> @name,
      'code'=>@code, 'quantity' => @quantity, 'discountCode'=>@discountCode,
      'price' => @price
      ),
      array(
      'brandCode' => @brandCode, 'article'=>@article, 'name'=> @name,
      'code'=>@code, 'quantity' => @quantity, 'discountCode'=>@discountCode,
      'price' => @price
      ),
      ),
      ),
      );

     */

    public function getProducts($configuration);

    /*  Получение данных о торговых точках 
      @param - массив кодов 1С контрагентов.
      @return (obj) - Торговые точки.

      array('@Код контрагента' => array(
      array(
      'pointCode' => '@Код точки1',
      'pointPhone' => '@Телефон точки1',
      'pointAddress' => '@Адрес точки1',
      'deliveries' => array(
      '@Код адреса доставки1' => '@Адрес доставки1',
      '@Код адреса доставки2' => '@Адрес доставки2',
      ),
      ),
      array(
      'pointCode' => '@Код точки2',
      'pointPhone' => '@Телефон точки2',
      'pointAddress' => '@Адрес точки2',
      'deliveries' => array(
      '@Код адреса доставки1' => '@Адрес доставки1',
      '@Код адреса доставки2' => '@Адрес доставки2',
      ),
      ),
      )
      );


     */

    public function getTradePoints($param);

    /*
      Получение остатка товаров

      смена наименований в массиве адреса доставки
      id на delivery_address_code
      name на delivery_address
      добавлено поле delivery_address_id
      @return (mixed) array(array('@productsCode'=>array('price'=>@float,'quantity'=>@int,'warehouse_id'=>@int,))); - Кол-во остатков
      @param (mixed) array(array('warehouse_id'=>@warehouse_id,'productCode'=> @productCode));
     */

    public function getProductsInfo($param);

    /*
      Синхронизация каталогов
      @return (obj) - Список каталогов, параметров каталога

      Example:
      $result = array(
      array('code'=>'0000001','name'=>'Масло','params'=>array(
      array('code'=>'10001','name'=>'Вязкость','values'=>array(
      array('code'=>'11001','name'=>'5w40'),
      array('code'=>'11002','name'=>'5w30'),
      ))
      )),
      );
     */

    public function getCatalogs();

    /*
      Синхронизация данных каталога.

      Example:
      $result = array(
      array('catalog' => '0000001'=>, 'data' => array(
      array('article'=>'OC90','brand'=>'00001','param'=>'10001','value'=>'11002'))
      );
     */

    public function getCatalogsData();

    /*
      Проверка связи с сервисом
      @return bool Результат подключения
     */

    public function initConnection();

    /*
      Получение данных контрагентах по Торговому представителю
      @param string Код1С.
      @return object Данные конртагентов
     */

    public function getContractorsByTrader($param);

    /*
      Получение данных контрагентах по Менеждеру
      @param string Код1С.
      @return object Данные конртагентов
     */

    public function getContractorsByManager($param);

    /*
      Получение данных контрагента
      @param string Код1С.
      @return object Данные конртагента
     */

    public function getContractor($param);

    /*
      Получение данных контрагентов
      @param array массив кодов.
      @return object Данные конртагентов
     */

    public function getContractors($param);

    /*
      Получение данных всех контрагентов покупателей
      @return object Данные конртагентов
     */

    public function getAllContractors();


    /*
     * @return (object) Измененные заказы + Статусы товаров
      структура возврата объекта
      foreach ($model as $order) {
      $return[$i]['orderId'] = $order->id;
      $return[$i]['clientId'] = $order->user_id;
      $return[$i]['clientCode'] = Users::model()->getCompanyCode($order->user_id);
      $return[$i]['deliveryMethod'] = $order->delivery->name;
      $return[$i]['deliveryAddress'] = $order->delivery_address;
      if ($order->products) {
      $j = 0;
      foreach ($order->products as $product) {
      $return[$i]['products'][$j]['product_id'] = (int) $product->id;
      $return[$i]['products'][$j]['article'] = $product->article;
      $return[$i]['products'][$j]['brand_id'] = (int) $product->brand_id;
      $return[$i]['products'][$j]['discount_id'] = (int) $product->discount_id;
      $return[$i]['products'][$j]['code'] = $product->code;
      $return[$i]['products'][$j]['quantity'] = $product->quantity;
      $return[$i]['products'][$j]['getted'] = $product->getted;
      $return[$i]['products'][$j]['status_id'] = (int) $product->status_id;
      $return[$i]['products'][$j]['last_update'] = $product->last_update;
      $j++;
      }
      }
      $i++;
      }

     */

    public function getOrders();

    /*
     * @param array('param'=>(object)) Новые заказы
     * @return (bool) Результат изменения 

      foreach ($model as $order) {
      $return[$i]['orderId'] = $order->id;
      $return[$i]['clientId'] = $order->user_id;
      $return[$i]['clientCode'] = Users::model()->getCompanyCode($order->user_id);
      $return[$i]['deliveryMethod'] = $order->delivery->name;
      $return[$i]['deliveryPrice'] = $order->delivery_price;
      $return[$i]['deliveryAddress'] = $order->delivery_address;
      $return[$i]['deliveryPrice'] = $order->delivery->price;
      $return[$i]['orderComment'] = $order->comment;
      $return[$i]['vat'] = $order->vat;
      $return[$i]['calculationType'] = $order->calculation->name;
      $return[$i]['orderDate'] = $order->datetime;
      if ($order->products) {
      $j = 0;
      foreach ($order->products as $product) {
      $return[$i]['products'][$j]['product_id'] = (int) $product->id;
      $return[$i]['products'][$j]['article'] = $product->article;
      $return[$i]['products'][$j]['brand_id'] = (int) $product->brand_id;
      $return[$i]['products'][$j]['discount_id'] = (int) $product->discount_id;
      $return[$i]['products'][$j]['code'] = $product->code;
      $return[$i]['products'][$j]['purchase_price'] = $product->purchase_price;
      $return[$i]['products'][$j]['price'] = $product->price;
      $return[$i]['products'][$j]['discount_price'] = $product->discount_price;
      $return[$i]['products'][$j]['quantity'] = $product->quantity;
      $return[$i]['products'][$j]['getted'] = $product->getted;
      $return[$i]['products'][$j]['status_id'] = (int) $product->status_id;
      $return[$i]['products'][$j]['comment'] = $product->comment;
      $return[$i]['products'][$j]['last_update'] = $product->last_update;
      $j++;
      }
      }
      $i++;
      }

     */

    public function setOrders($orders);

    /*
      Добавлено поле "delivery_address_code" и "delivery_address_id"
      в products добавлено поле "param"
      Переименовал "deliveryAddress" на "delivery_address"
     * @param (string) Ключ принятых заказов
     * @return (bool) Результат обработки
     */

    public function confirmOrders();

    /*
     * @param (string) Код1С клиента
     * @return (object) 
      doc_id
      doc_price
      doc_payday
      doc_date
     */

    public function getDebt($client_d);
    /*
     * @return (obj) Скидки клиентов
     */

    public function getDiscounts();

    /*
     * @return (obj) Дискретные скидки
     */

    public function getDiscreteDiscounts();

    /*
     * @param userID(string) Ид-пользователя-физ лица
     * @param clientID(string) Ид-контрагента
     * @param discountBrandID(int) Скидочный бренд 
     * @param discountID(string) Скидка
     * @return (bool) Результат обработки

     */

    public function setDiscount($userID, $clientID, $discountBrandID, $discountID);

    /*
     * @param (obj) Список товаров (1С-код)
     * @return (obj) [1С-код => количество]

      Example:
      array(
      'param'=>array(
      'data'=>array(
      'positions'=>$positions)
      ));

     */

    public function getCartPositions($positions);




    /*
     * 
     * @return (obj) Список статусов товара [Ключ_статуса]=>[Значение статуса]
     * 
      array('statuses'=>array(
      array(
      'id'=>'[Код статуса в 1С]',
      'name'=>'[Название статуса]'
      ));
     */

    public function getProductStatuses();
}
