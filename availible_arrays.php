<?php

/**
 * 24.06.2014
 * @method public getGroups() 
 * Выгрузка справочников
 * 
 * @property array $brands Бренды
 * @property array $priceGroups Ценовые группы
 * @property array $priceTypes Типы цен
 * @property array $nomenclatureGroups Номенклатурые группы
 * 
 */
array(
    'brands' => array(
        array(
            'code' => '@code',
            'name' => '@name'
        )
    ),
    'priceGroups' => array(
        array(
            'code' => '@code',
            'brandCode' => '@brandCode',
            'name' => '@name'
        )
    ),
    'priceTypes' => array(
        array(
            'code' => '@code',
            'name' => '@name',
            'priceCode' => "@priceGroupCode",
            'value' => '@value'
        )
    ),
    'nomenclatureGroups' => array(
        array(
            'code' => '@code',
            'name' => '@name',
            'parentCode' => '@parent',
        )
    ),
);

/**
 * 24.06.2014
 * @method public getDiscounts() 
 * Выгрузка скидок клиентов
 * 
 * @property array $legalCode Код контрагента
 * @property array $priceCode Код ценовой группы
 * @property array $priceTypeCode Код типа цен
 * 
 */
array(
    array(
        'legalCode' => '@legalCode',
        'priceCode' => '@priceCode',
        'priceTypeCode' => '@priceTypeCode'
    )
);

/**
 * 24.06.2014
 * @method public getProducts() 
 * Выгрузка номенклатуры
 * 
 * @property array $article Артикул
 * @property array $brandCode Бренд
 * @property array $name Наименование
 * @property array $code Код 
 * @property array $priceCode Ценовая группа
 * @property array $nomenclatureCode Номенклатурная группа
 * @property array $multiplicity Кратность
 * 
 */
array(
    array(
        'article' => '@article',
        'brandCode' => '@brandCode',
        'name' => '@name',
        'code' => '@code',
        'priceCode' => '@priceCode',
        'nomenclatureCode' => '@nomenclatureCode',
        'multiplicity' => '@multiplicity',
    )
);

/**
 * 24.06.2014
 * @method public getProductsPositions(warehousePrefix) 
 * @param warehousePrefix Префикс слада
 * Выгрузка остатков
 * 
 * @property array $article Артикул
 * @property array $quantity Количество
 * @property array $price Цена
 * 
 */
array(
    array(
        'code' => '@article_code',
        'quantity' => '@quantity',
        'price' => '@price',
    )
);

/**
 * 05.06.2014
 * Начало: Допустимый массив setVisits (обмен данными по RM)  
 */
array(
    array(
        'managerCode' => '@managerCode',
        'pointCode' => '@pointCode',
        'time' => array(
            array(
                'start' => '@start',
                'end' => '@end',
            ),
            array(
                'start' => '@start',
                'end' => '@end',
            )
        )
    )
);
/* Конец */


/**
 * 03.06.2014
 * Начало: Допустимый массив getRm (обмен данными по RM)  
 */
array(
    'quality' => array(
        array(
            'quality_code' => '@code',
            'quality_name' => '@name'
        ),
    ),
    'routes' => array(
        array(
            'route_code' => '@code', // Код маршрута
            'route_name' => '@name' // Наименование маршрута
        )
    ),
    'actions' => array(// Массив активностей
        array(
            'action_code' => '@code', // Код активности
            'action_name' => '@name', // Наименование активности
            'action_values' => array(// Массив допустимых значений активности
                'value_code' => '@code', // Код значения активности
                'value_name' => '@name' // Наименование значения активности
            )
        )
    )
);
/* Конец */

/**
 * @TODO: Описать массив
 * 04.06.2014
 * Начало: Допустимый массив setActivities (Отправка данных об активностиях в 1С)  
 */
array(
    'activities' => array(
        array(
            'managerCode' => '@manager_code',
            'points' => array(
                'date' => '@date',
                'point_code' => '@pointCode',
                'values' => array(
                    array(
                        'activityCode' => '@activityCode',
                        'activityValueCode' => '@activityValueCode'
                    )
                )
            ),
        )
    )
);
/* Конец */

/**
 * @TODO: Описать массив
 * 03.06.2014
 * Начало: Допустимый массив getRoutes (Маршрутный лист)  
 */
array(
    'elements' => array(
        array(
            'managerCode' => '@managerCode', // Код менеджера
            'routes' => array(// Массив маршрутных точек
                array(
                    'routeCode' => '@routeCode', // Код маршрута
                    'tradePoints' => array(// Массив торговых точек
                        array(
                            'tradeCode' => '@tradeCode', // Код торговой точки
                            'index' => '@sortIndex' // Индекс сортировки
                        )
                    ),
                )
            )
        ),
    ),
);
/* Конец */
